﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DesignPatterns.Actions;

namespace DesignPatterns
{
    class DrawingCanvas : Canvas
    {
        static DrawingCanvas _instance;       
        private DrawingCanvas()
        {
            HorizontalAlignment = HorizontalAlignment.Left;
            VerticalAlignment = VerticalAlignment.Top;
            Height = 740;
            Width = 1272;
            Margin = new Thickness(0, 29, 0, 0);
            Background = Brushes.LightYellow;
        }
        public static DrawingCanvas GetInstance()
        {
            if (_instance == null)
            {
                _instance = new DrawingCanvas();
            }
            return _instance;
        }
    }
}
