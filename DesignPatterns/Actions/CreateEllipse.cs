﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DesignPatterns.Actions;
namespace DesignPatterns.Actions
{
    class CreateEllipse : IAction
    {
        public void Execute()
        {
            Ellipse ellipse = new Ellipse();
            ellipse.Width = 20;
            ellipse.Height = 20;
            ellipse.Margin = new Thickness(0, 0, 0, 0);
            ellipse.Fill = Brushes.Red;
            DrawingCanvas.GetInstance().Children.Add(ellipse);
            Console.WriteLine("Created ellipse.");
        }
    }
}
