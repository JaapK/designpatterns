﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Actions
{
    class ActionExecuter
    {
        List<IAction> Actions = new List<IAction>();
        
        public void AddAction(IAction a)
        {
            Actions.Add(a);
        }
        public void DoAction()
        {
            Actions.Last().Execute();
        }
    }
}
