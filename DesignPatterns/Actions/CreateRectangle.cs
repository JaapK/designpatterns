﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DesignPatterns.Actions;

namespace DesignPatterns.Actions
{
    class CreateRectangle : IAction
    {
        public void Execute()
        {
            Rectangle rect = new Rectangle();
            rect.Width = 20;
            rect.Height = 20;
            rect.Margin = new Thickness(0, 0, 0, 0);           
            rect.Fill = Brushes.Blue;
            DrawingCanvas.GetInstance().Children.Add(rect);
            Console.WriteLine("Created rectangle.");
        }
    }
}
