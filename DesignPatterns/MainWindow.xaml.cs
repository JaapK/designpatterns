﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DesignPatterns.Actions;

namespace DesignPatterns
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ActionExecuter actionExecuter = new ActionExecuter();
        //<Canvas HorizontalAlignment="Left" Height="740" VerticalAlignment="Top" Width="1272" Margin="0,29,0,0" Background="#FFE2D5D5"/>
        DrawingCanvas canvas = DrawingCanvas.GetInstance();                
        public MainWindow()
        {
            InitializeComponent();
            canvas.HorizontalAlignment = HorizontalAlignment.Left;
            canvas.VerticalAlignment = VerticalAlignment.Top;
            canvas.Height = 740;
            canvas.Width = 1272;
            canvas.Margin = new Thickness(0, 29, 0, 0);
            canvas.Background = Brushes.LightYellow;
            this.grid.Children.Add(canvas);

        }
        private void Rectangle_Click(object sender, RoutedEventArgs e)
        {
            actionExecuter.AddAction(new CreateRectangle());
            actionExecuter.DoAction();
        }

        private void Ellipse_Click(object sender, RoutedEventArgs e)
        {
            actionExecuter.AddAction(new CreateEllipse());
            actionExecuter.DoAction();
        }
    }
}
